﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcShopping.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        // 显示完成订单的窗体页面
        public ActionResult Complete()
        {
            return View();
        }

        //将订单信息于购物车信息写入数据库
        [HttpPost]
        public ActionResult Complete(FormCollection form)
        {
            //TODO: 将订单信息于购物车信息写入数据库

            //TODO: 订单完成后必须清空现有的购物车信息

            //订单完成后回到网站首页
            return RedirectToAction("Index","Home");
        }

    }
}