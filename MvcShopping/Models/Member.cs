﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcShopping.Models
{
    [DisplayName("会员信息")]
    [DisplayColumn("Name")]
    public class Member
    {
        [Key]
        public int Id { get; set; }

        [DisplayName("会员账号")]
        [Required(ErrorMessage ="请输入Email地址")]
        [Description("Email作为会员的登录账号")]
        [MaxLength(250,ErrorMessage ="Email地址长度不可超过250字")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }


        [DisplayName("会员密码")]
        [Required(ErrorMessage = "请输入密码")]
        [Description("密码将以SHA1进行哈希运算,通过运算后转为HEX表的结果长度为40字符")]
        [MaxLength(40, ErrorMessage = "密码长度不可超过40字")]
        [DataType(DataType.Password)]
        public string Password { get; set; }


        [DisplayName("中文姓名")]
        [Required(ErrorMessage = "请输入中文姓名")]
        [Description("暂不考虑有外国人用英文注册")]
        [MaxLength(5, ErrorMessage = "中文姓名长度不可超过5字")]
        public string Name { get; set; }

        [DisplayName("网络姓名")]
        [Required(ErrorMessage = "请输入网络姓名")]
        [MaxLength(10, ErrorMessage = "网络姓名长度不可超过10字")]
        public string NickName { get; set; }

        [DisplayName("会员注册时间")]
        public DateTime RegisterOn { get; set; }


        [DisplayName("会员启用认证码")]
        [MaxLength(36)]
        [Description("当AuthCode为Null,则标识此会员已经通过Email有效性验证")]
        public string AuthCode { get; set; }

    }
}