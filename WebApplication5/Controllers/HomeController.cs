﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            BaseContext baseContext = new BaseContext();
            baseContext.Account.Add(new Account { Name = "asdsad" });
    
            baseContext.Test.Add(new Test { Name="asddadasda"});
            baseContext.SaveChanges();



            return View();
        }

        public ActionResult About()
        {
            BaseContext baseContext = new BaseContext();
            ViewBag.NAME = baseContext.Account.FirstOrDefault()?.Name;
            ViewBag.NAME2 = baseContext.Test.FirstOrDefault()?.Name;
            ViewBag.Message = "Your application description page.";
  
            return View();
        }
        public ActionResult About2()
        {

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}